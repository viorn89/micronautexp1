package com.example.repositories;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import com.example.entities.Post;
import com.example.entities.PostList;

import io.reactivex.Single;

@Singleton
public class PostRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public PostRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public PostList getPostList(Integer offsetId) {
        int mOffsetId = -1;
        if (offsetId != null)
            mOffsetId = offsetId;
        String qlString = "SELECT p FROM Post as p WHERE p.id>:offsetId";
        TypedQuery<Post> query = entityManager.createQuery(qlString, Post.class).setParameter("offsetId", mOffsetId);
        query.setMaxResults(5);

        String lastIdSQL = "SELECT p from Post as p ORDER BY id DESC";
        TypedQuery<Post> lastIdQuery = entityManager.createQuery(lastIdSQL, Post.class);
        int lastId = -1;
        Post lastItem = lastIdQuery.setMaxResults(1).getSingleResult();
        if (lastItem != null)
            lastId = lastItem.getId();

        return new PostList(query.getResultList(), lastId);
    }

    @Transactional
    public PostList getPostListDesc(Integer offsetId) {
        int mOffsetId = Integer.MAX_VALUE;
        if (offsetId != null)
            mOffsetId = offsetId;
        String qlString = "SELECT p FROM Post as p WHERE p.id<:offsetId ORDER BY id DESC";
        TypedQuery<Post> query = entityManager.createQuery(qlString, Post.class).setParameter("offsetId", mOffsetId);
        query.setMaxResults(5);

        String firstIdSQL = "SELECT p from Post as p ORDER BY id ASC";
        TypedQuery<Post> firstIdQuery = entityManager.createQuery(firstIdSQL, Post.class);
        int firstId = -1;
        Post firstItem = firstIdQuery.setMaxResults(1).getSingleResult();
        if (firstItem != null)
            firstId = firstItem.getId();

        return new PostList(query.getResultList(), firstId);
    }

    public Single<PostList> singlePostList(Integer offsetId) {
        return Single.fromCallable(new Callable<PostList>() {
            @Override
            public PostList call() throws Exception {
                return getPostList(offsetId);
            }
        });
    }

    public Single<PostList> singlePostListDesc(Integer offsetId) {
        return Single.fromCallable(new Callable<PostList>() {
            @Override
            public PostList call() throws Exception {
                return getPostListDesc(offsetId);
            }
        });
    }

}