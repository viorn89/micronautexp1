package com.example.controllers;

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;

import com.example.entities.Post;
import com.example.entities.PostList;
import com.example.repositories.PostRepository;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.QueryValue;
import io.reactivex.Observable;
import io.reactivex.Single;

@Controller("/posts")
public class PostController {

    PostRepository postRepository;

    public PostController(PostRepository postRepository) {
        this.postRepository = postRepository;
    }
    

    @Get(value = "list", produces = MediaType.APPLICATION_JSON)
    Single<PostList> getList(@Nullable @QueryValue("offsetId") Integer offsetId) {
        return postRepository.singlePostListDesc(offsetId);
    }
}