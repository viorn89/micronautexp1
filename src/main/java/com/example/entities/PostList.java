package com.example.entities;

import java.util.List;

public class PostList {
    private List<Post> list;
    private int lastId;

    public PostList() {

    }

    public PostList(List<Post> list, int lastId) {
        this.list = list;
        this.lastId = lastId;
    }

    public List<Post> getList() {
        return list;
    }

    public void setList(List<Post> list) {
        this.list = list;
    }

    public int getLastId() {
        return lastId;
    }

    public void setLastId(int lastId) {
        this.lastId = lastId;
    }

}