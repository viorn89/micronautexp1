package com.example;

import java.math.BigInteger;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.example.entities.Post;

import io.micronaut.discovery.event.ServiceReadyEvent;
import io.micronaut.runtime.event.annotation.EventListener;

@Singleton
public class MockDataLoader {

    @PersistenceContext
    private EntityManager entityManager;

    public MockDataLoader(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @EventListener
    void onReady(ServiceReadyEvent event) {
        fillMockData();
    }    

    @Transactional
    void fillMockData() {
        BigInteger count = (BigInteger) entityManager.createNativeQuery("SELECT count(1) FROM Posts").getSingleResult();
        if (count.longValue()>0)return;

        entityManager.persist(new Post(1, "test1", "body1", ""));
        entityManager.persist(new Post(2, "test2", "body2", ""));
        entityManager.persist(new Post(1, "test3", "body3", ""));
        entityManager.persist(new Post(3, "test4", "body4", ""));
        entityManager.persist(new Post(1, "test5", "body5", ""));

        entityManager.persist(new Post(1, "test6", "body6", ""));
        entityManager.persist(new Post(2, "test7", "body7", ""));
        entityManager.persist(new Post(1, "test8", "body8", ""));
        entityManager.persist(new Post(3, "test9", "body9", ""));
        entityManager.persist(new Post(1, "test10", "body10", ""));

        entityManager.persist(new Post(1, "test11", "body11", ""));
        entityManager.persist(new Post(2, "test12", "body12", ""));
        entityManager.persist(new Post(1, "test13", "body13", ""));
        entityManager.persist(new Post(3, "test14", "body14", ""));
        entityManager.persist(new Post(1, "test15", "body15", ""));
    }
}